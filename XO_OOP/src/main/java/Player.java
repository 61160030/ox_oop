
public class Player {

    private char name;
    private int win, lose, draw;

    public char getName() {

        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public void addWin() {
        win++;
    }

    public void addLose() {
        lose++;
    }

    public void addDraw() {
        draw++;
    }
}
