
import java.util.Scanner;

public class Game {

    private int row;
    private int col;
    private Player X;
    private Player O;
    private Table table;
    private char continue1 = 'Y';

    public Game() {
        X = new Player();
        X.setName('X');
        O = new Player();
        O.setName('O');
    }

    public void startGame() {
        table = new Table(O, X);

    }

    public void showWelcome() {
        System.out.println("Welcome to XO game");
    }

    public void showTable() {
        System.out.println(table.getTableString());
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " Turn plz enter row and col : ");
    }

    public void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        do {

            row = kb.nextInt();
            col = kb.nextInt();
        } while (table.checkPosition(row, col));
    }

    public void inputContunue() {
        Scanner kb = new Scanner(System.in);
        System.out.println("Do you want to play more Y or N");
        continue1 = kb.next().charAt(0);
    }

    public void endTurn() {
        System.out.println("________________");

    }

    public void showScore() {
        System.out.println("X Win:" + X.getWin() + " Lost:" + X.getLose()
                + " Draw:" + X.getDraw());
        System.out.println("O Win:" + O.getWin() + " Lost:" + O.getLose()
                + " Draw:" + O.getDraw());
    }

    public void showWinner() {
        System.out.println(table.getWinner().getName() + " Win");
    }

    public void run() {
        Scanner kb = new Scanner(System.in);
        while (continue1 == 'Y') {
            startGame();
            showWelcome();

            do {
                showTable();
                showTurn();
                inputRowCol();
                table.setRowCol(row, col);
                endTurn();
                table.swithTurn();
            } while (table.checkWin(row, col) == false && table.getCountTurn() < 9);
            showTable();

            if (table.checkWin(row, col)) {
                showWinner();
                table.updateScore();

            }
            showScore();
            inputContunue();
            if (continue1 == 'N') {
                showBye();
            }
        }
    }

    public void showBye() {
        System.out.println("Good bye");
    }

}
