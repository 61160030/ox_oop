
public class Table {

    private char[][] table = {
        {'_', '_', '_'},
        {'_', '_', '_'},
        {'_', '_', '_'}
    };
    private Player O;
    private Player X;
    private Player currentPlayer;
    private Player winner;
    private int countTurn = 0;

    public Table(Player O, Player X) {
        this.O = O;
        this.X = X;
        currentPlayer = X;

    }

    public void setRowCol(int row, int col) {
        
    table[row - 1][col - 1]=currentPlayer.getName();
    countTurn++;
    }

    public Player getWinner() {
        return winner;

    }

    public void swithTurn() {
        if (currentPlayer == X) {
            currentPlayer = O;
        } else {
            currentPlayer = X;
        }
    }

    public Player getCurrentPlayer() {

        return currentPlayer;

    }

    public char[][] getData() {

        return table;
    }

    public String getTableString() {

        return "|" + table[0][0] + "|" + table[0][1] + "|" + table[0][2] + "| \n"
                + "|" + table[1][0] + "|" + table[1][1] + "|" + table[1][2] + "| \n"
                + "|" + table[2][0] + "|" + table[2][1] + "|" + table[2][2] + "|";
    }
    public boolean checkPosition(int row, int col) {
        if ((row > 3 || col > 3) || (row < 1 || col < 1)) {
            System.out.println("re enter the collect row and col");
            return true;
        }
        else if (table[row - 1][col - 1] != '_' ) {
            System.out.println("This position isn't empty!! Please try again.");
            return true;
        }
        return false;
    }
    public void updateScore() {
        if (winner.getName() == O.getName()) {
            O.addWin();
            X.addLose();
        } else if (winner.getName() == X.getName()) {
            X.addWin();
            O.addLose();
        }
    }
    public int getCountTurn() {
        return countTurn;
    }
    

    public boolean checkRow(int row) {
        if (table[row-1][0] == table[row-1][1]&& table[row - 1][0] != '_') {
            if (table[row-1][1] == table[row-1][2]) {
                return true;
            }
        }

        return false;

    }

    public boolean checkCol(int col) {
        if (table[0][col-1] == table[1][col-1]&& table[0][col - 1] != '_') {
            if (table[1][col-1] == table[2][col-1]) {
                return true;
            }
        }

        return false;

    }

    public boolean cheakX1() {
        if(table[0][0] == table[1][1] && table[0][0] != '_'){
            if(table[1][1] == table[2][2]){
            return true;
        }
        }

        return false;

    }

    public boolean cheakX2() {
        if(table[0][2] == table[1][1]&& table[0][2] != '_'){
            if(table[1][1] == table[0][2]){
            return true;
        }
        }

        return false;

    }
    public boolean checkWin(int row, int col) {
        if (checkRow(row)) {
            winner = currentPlayer;
            swithTurn();
            return true;
        } else if (checkCol(col)) {
            winner = currentPlayer;
            swithTurn();
            return true;
        } else if (cheakX1()) {
            winner = currentPlayer;
            swithTurn();
            return true;
        } else if (cheakX2()) {
            winner = currentPlayer;
            swithTurn();
            return true;
        } else if (countTurn == 9) {
            countTurn++;
            O.addDraw();
            X.addDraw();
            return false;
        } else {
            return false;
        }
    }
    

}
